create table if not exists companies (
    id   bigint auto_increment primary key,
    name varchar(255) null
);


create table if not exists employees (
    id         bigint auto_increment primary key,
    age        int          null,
    company_id bigint       null,
    gender     varchar(255) null,
    name       varchar(255) null,
    salary     int          null,
    foreign key (company_id) references companies (id)
);