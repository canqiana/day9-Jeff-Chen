package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return jpaEmployeeRepository.findAll()
                .stream()
                .map(EmployeeMapper::entityToResponse)
                .collect(Collectors.toList());
    }

    public Employee findById(Long id) {
        return jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
    }

    public EmployeeResponse findEmployeeById(Long id) {
        return EmployeeMapper.entityToResponse(findById(id));
    }

    public EmployeeResponse update(Long id, EmployeeRequest employeeRequest) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        return EmployeeMapper.entityToResponse(jpaEmployeeRepository.save(toBeUpdatedEmployee));
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        return jpaEmployeeRepository.findByGender(gender)
                .stream()
                .map(EmployeeMapper::entityToResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.requestToEntity(employeeRequest);
        return EmployeeMapper.entityToResponse(jpaEmployeeRepository.save(employee));
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        return jpaEmployeeRepository.findAll(PageRequest.of(page-1, size)).getContent()
                .stream()
                .map(EmployeeMapper::entityToResponse)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
