package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static Company requestToEntity(CompanyRequest companyRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest,company);
        return company;
    }

    public static CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        companyResponse.setEmployeesCount(company.getEmployees() != null ? company.getEmployees().size() : 0);
        return companyResponse;
    }

    public static Company responseToEntity(CompanyResponse companyResponse) {
        Company company = new Company();
        BeanUtils.copyProperties(companyResponse,company);
        return company;
    }
}
