package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeMapper {
    public static Employee requestToEntity(EmployeeRequest employeeRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRequest,employee);
        return employee;
    }

    public static EmployeeResponse entityToResponse(Employee employee) {
        EmployeeResponse employeeRespnse = new EmployeeResponse();
        BeanUtils.copyProperties(employee,employeeRespnse);
        return employeeRespnse;
    }

    public static List<EmployeeResponse> toResponseList(List<Employee> employees) {
        return employees.stream().map(EmployeeMapper::entityToResponse).collect(Collectors.toList());
    }
}
