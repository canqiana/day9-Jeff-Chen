package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private JPACompanyRepository jpaCompanyRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return jpaCompanyRepository.findAll(PageRequest.of(page-1, size)).getContent();
    }

    public Company findById(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public CompanyResponse findCompanyById(Long id) {
        return CompanyMapper.toResponse(findById(id));
    }

    public CompanyResponse update(Long id, Company company) {
        Company updateCompany = findById(id);
        updateCompany.setName(company.getName());
        return CompanyMapper.toResponse(jpaCompanyRepository.save(updateCompany));
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.requestToEntity(companyRequest);
        return CompanyMapper.toResponse(jpaCompanyRepository.save(company));
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        return EmployeeMapper.toResponseList(findById(id).getEmployees());
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
