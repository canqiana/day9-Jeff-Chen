Objective: 

1.  Code Review : review each other's homework with my teammates , Incorporate feedback from teammates to modify my code, such as removing logically duplicated code.
1.  Mapper: use employee mapper to transform employeeRequest to employee entity and transform employee entity to employeeResponse.
1.  Learn database version management tool: Flyway, and use it to create tables and insert data.
1.  Microservice Session: share advantages and disadvantages of microservices architecture.

Reflective:  Fruitful .

Interpretive:  During this day of study, I learned a lot about Spring Boot Mapper and Cloud Native.

Decisional:   I'm going to learn more about Spring Boot and JPA. I will also actively seek the help of teachers and classmates when I have problem.
