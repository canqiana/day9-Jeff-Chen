Objective: 

1.  Code Review : review each other's homework with my teammates , Incorporate feedback from teammates to modify my code, such as modifying the integration tests and unit tests code.
1. SQL Basic: review DML create-delete-update-select and DDL  alter-drop-create.
1. JPA: learn JPA and practice it in employee and company demo.

Reflective:  Fruitful and Challenging.

Interpretive:  During this day of study, I learned a lot about  Integration Test and JPA, and I met some difficulties in the process of writing unit test and integration test.

Decisional:  I'm going to learn more about unit test ,integration test and JPA!
